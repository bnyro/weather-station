#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include "config.h"

// Config file content:
// const char ssid[] = "";
// const char password[] = "";
// String host = "http://example.com/api/weather/latest?format=csv";

String datetime, temperature, humidity, pressure;

WiFiClient client;
LiquidCrystal_I2C lcd(0x27, 20, 4);

void setup()
{
  pinMode(D0, INPUT_PULLUP);
  WiFi.mode(WIFI_STA);

  lcd.init();
  lcd.backlight();

  connectWiFi();
  GetWeatherData();
  PrintWeatherData();

  delay(10000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Press me twice to");
  lcd.setCursor(0, 1);
  lcd.print("power off. NOW!");
}

void loop() {}

void connectWiFi()
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Connecting to ");
  lcd.setCursor(0, 1);
  lcd.print(ssid);

  while (WiFi.status() != WL_CONNECTED) {
    WiFi.begin(ssid, password);
    delay(5000);
  }

  lcd.setCursor(0, 2);
  lcd.print("Successfully connected.");
  delay(1000);
}

void GetWeatherData()
{
  HTTPClient http;

  http.begin(client, host);

  int response = http.GET();
  String csv = http.getString();

  datetime = getValue(csv, ',', 0);
  temperature = getValue(csv, ',', 1);
  humidity = getValue(csv, ',', 2);
  pressure = getValue(csv, ',', 3);
}

void PrintWeatherData()
{
  lcd.clear();

  lcd.setCursor(0, 0);
  lcd.print(datetime);

  lcd.setCursor(0, 1);
  lcd.print("Temperatur: " + temperature + "C");

  lcd.setCursor(0, 2);
  lcd.print("Luftfeuchte: " + humidity + "%");

  lcd.setCursor(0, 3);
  lcd.print("Luftdruck: " + pressure + "hPa");
}

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
